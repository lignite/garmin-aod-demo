using Toybox.Graphics;
using Toybox.WatchUi as Ui;

class Watchface extends Ui.WatchFace {
  var custom_font = null;

  var amoled_aod = false;
  var amoled_aod_shift = false;

  function initialize() {
    WatchFace.initialize();
    custom_font = Ui.loadResource(Rez.Fonts.bebas_medium);
  }

  function onEnterSleep(){
    System.println("> Enter sleep");
    amoled_aod = true;
  }

  function onExitSleep(){
    System.println(">Leave sleep");
    amoled_aod = false;
  }

  function onUpdate(dc) {
    System.println("Drawing, amoled_aod: " + amoled_aod);

    dc.setColor(0x000000, 0x000000);
    dc.clear();



    var centre = System.getDeviceSettings().screenWidth / 2;
    dc.setColor(0xFF0000, Graphics.COLOR_TRANSPARENT);

    dc.fillCircle(centre, centre, 40);

    dc.drawText(centre, 100, Graphics.FONT_LARGE, "system font", (Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER));

    dc.drawText(centre, 300, custom_font, "custom font", (Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER));



    if(amoled_aod){
      drawAodLines(dc, 1);
    }
  }

  function drawAodLines(dc, width){
    dc.setPenWidth(width);
    dc.setColor(0x000000, Graphics.COLOR_TRANSPARENT);

    for(var i = (amoled_aod_shift ? 1 : 0); i < 416; i += (1 + width)){
      dc.drawLine(0, i, 416, i);
    }

    amoled_aod_shift = !amoled_aod_shift;
  }
}
