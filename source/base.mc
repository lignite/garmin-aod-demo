using Toybox.Time;
using Toybox.WatchUi;
using Toybox.Background;
using Toybox.Application;

class WatchfaceBase extends Application.AppBase {
  var watchface;

  function initialize() {
    AppBase.initialize();
  }

  function onStart(state) {
    System.println("onStart");
  }

  function onStop(state) {
    System.println("onStop");
  }

  function getInitialView() {
    watchface = new Watchface();

    return [ watchface ];
  }
}
